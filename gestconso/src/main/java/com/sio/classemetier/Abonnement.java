package com.seg.gestconso.metier;

import java.util.*;
import javax.persistence.*;

/**
 * Un abonnement représente un logement raccordé au service
 *
 * @author sio
 */
@Entity
public class Abonnement {

//<editor-fold defaultstate="collapsed" desc="Propriétés">
    /**
     * Référence unique et définitive de l'abonnement
     */
    @Id
    private String ref;
    
    /**
     * Date de création de l'abonnement
     */
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateAbonnement;
    
    /**
     * Adresse du logement concerné par l'abonnement
     */
    @Column(length = 150)
    private String adresse;
    
    /**
     * Code postal du logement concerné par l'abonnement
     */
    @Column(length = 5)
    private String cp;
    
    /**
     * Ville dans laquelle se situe le logement concerné par l'abonnement
     */
    @Column(length = 38)
    private String ville;
    
    /**
     * Client propriétaire de l'abonnement.
     */
    @ManyToOne
    @JoinColumn(name = "idClient")
    private Client leClient;
    
    /**
     * Les compteurs usager de l'abonnement
     */
    @OneToMany(mappedBy = "lAbonnement")
    private List<Usager> lesCompteursUsager = new ArrayList<>();
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructeur">
    /**
     * Crée un abonnement
     */
    public Abonnement() {
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Accesseurs">
    /**
     * Retourne la référence de l'abonnement
     *
     * @return La référence de l'abonnement
     */
    public String getRef() {
        return ref;
    }
    
    /**
     * Retourne la date de création du contrat
     *
     * @return La date de création du contrat
     */
    public Date getDateAbonnement() {
        return dateAbonnement;
    }
    
    /**
     * Retourne l'adresse du logement concerné par le contrat
     *
     * @return L'adresse du logement concerné par le contrat
     */
    public String getAdresse() {
        return adresse;
    }
    
    /**
     * Retourne le code postal du logement concerné par le contrat
     *
     * @return Le code postal du logement concerné par le contrat
     */
    public String getCp() {
        return cp;
    }
    
    /**
     * Retourne la ville du logement concerné par le contrat
     *
     * @return La ville du logement concerné par le contrat
     */
    public String getVille() {
        return ville;
    }
    
    /**
     * Retourne le numéro de client concerné par le contrat
     *
     * @return Le numéro du client concerné par le contrat
     */
    public Client getLeClient() {
        return leClient;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Mutateurs">
    /**
     * Met à jour la référence de l'abonnement (méthode obligatoire pour le
     * fonctionnement d'Hibernate)
     *
     * @param ref La nouvelle référence de l'abonnement
     */
    public void setRef(String ref) {
        this.ref = ref;
    }
    
    /**
     * Met à jour la date de création du contrat
     *
     * @param dateAbonnement La nouvelle date de création de contrat
     */
    public void setDateAbonnement(Date dateAbonnement) {
        this.dateAbonnement = dateAbonnement;
    }
    
    /**
     * Met à jour l'adresse du logement concerné par le contrat
     *
     * @param adresse La nouvelle adresse du logement concerné par le contrat
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    /**
     * Met à jour le code postal du logement concerné par le contrat
     *
     * @param cp Le nouveau code postal du logement concerné par le contrat
     */
    public void setCp(String cp) {
        this.cp = cp;
    }
    
    /**
     * Met à jour la ville du logement concerné par le contrat
     *
     * @param ville La nouvelle ville du logement concerné par le contrat
     */
    public void setVille(String ville) {
        this.ville = ville;
    }
    
    /**
     * Met à jour le numéro du client concerné par le contrat
     *
     * @param leClient Le nouveau numéro de client concerné par le contrat
     */
    public void setLeClient(Client leClient) {
        this.leClient = leClient;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Méthode">
    /**
     * Permet d'ajouter un compteur usager à la liste des compteurs
     *
     * @param u Le compteur usager à ajouter
     */
    public void ajouterCompteur(Usager u) {
        u.setlAbonnement(this);
        lesCompteursUsager.add(u);
    }
//</editor-fold>
}
