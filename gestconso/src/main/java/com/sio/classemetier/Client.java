package com.seg.gestconso.metier;

import java.util.*;
import javax.persistence.*;

/**
 * Un client représente la personne qui a souscrit un contrat (Un même client peut avoir plusieurs abonnements)
 * @author sio
 */
@Entity
public class Client {
    
//<editor-fold defaultstate="collapsed" desc="Propriétés">
    /**
     * Identifiant unique et définitif du client
     */
    @Id
    private int numClient;
    
    /**
     * Le nom de famille du client
     */
    @Column(length=36)
    private String nomClient;
    
    /**
     * Le prénom du client
     */
    @Column(length=36)
    private String prenomClient;
    
    /**
     * L'adresse personnelle du client
     */
    @Column(length=150)
    private String adresseClient;
    
    /**
     * Le code postal de l'adresse personnelle du client
     */
    @Column(length=5)
    private String cpClient;
    
    /**
     * La ville du logement personnel du client
     */
    @Column(length=38)
    private String villeClient;
    
    /**
     * Liste des abonnements que possède le client
     */
    @OneToMany(mappedBy="leClient")
    private List<Abonnement> lesAbonnements = new ArrayList<>();
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Constructeur">
    /**
     * Crée un client
     */
    public Client() {
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Accesseurs">
    /**
     * Retourne l'identifiant du client
     *
     * @return L'identifiant du client
     */
    public int getNumClient() {
        return numClient;
    }
    
    /**
     * Retourne le nom de famille du client
     *
     * @return Le nom de famille du client
     */
    public String getNomClient() {
        return nomClient;
    }
    
    /**
     * Retourne le prénom du client
     *
     * @return Le prénom du client
     */
    public String getPrenomClient() {
        return prenomClient;
    }
    
    /**
     * Retourne l'adresse personnelle du client
     *
     * @return L'adresse personnelle du client
     */
    public String getAdresseClient() {
        return adresseClient;
    }
    
    /**
     * Retourne le code postal de l'adresse personnelle du client
     *
     * @return Le code postal de l'adresse personnelle du client
     */
    public String getCpClient() {
        return cpClient;
    }
    
    /**
     * Retourne la ville de l'adresse personnelle du client
     *
     * @return La ville de l'adresse personnelle du client
     */
    public String getVilleClient() {
        return villeClient;
    }
    
    /**
     * Retourne la liste des abonnements d'un client
     *
     * @return lesAbonnements
     */
    public List<Abonnement> getLesAbonnements() {
        return lesAbonnements;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Mutateurs">
    /**
     * Met à jour l'identifiant du client
     *
     * @param numClient Le nouvel identifiant du client
     */
    public void setNumClient(int numClient) {
        this.numClient = numClient;
    }
    
    /**
     * Met à jour le nom de famille du client
     *
     * @param nomClient Le nouveau nom de famille du client
     */
    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }
    
    /**
     * Met à jour le prénom du client
     *
     * @param prenomClient Le nouveau prénom du client
     */
    public void setPrenomClient(String prenomClient) {
        this.prenomClient = prenomClient;
    }
    
    /**
     * Met à jour l'adresse personnelle du client
     *
     * @param adresseClient La nouvelle adresse du client
     */
    public void setAdresseClient(String adresseClient) {
        this.adresseClient = adresseClient;
    }
    
    /**
     * Met à jour le code postal de l'adresse personnelle du client
     *
     * @param cpClient Le nouveau code postal de l'adresse personnelle du client
     */
    public void setCpClient(String cpClient) {
        this.cpClient = cpClient;
    }
    
    /**
     * Met à jour la ville de l'adresse personnelle du client
     *
     * @param villeClient La nouvelle ville de l'adresse personnelle du client
     */
    public void setVilleClient(String villeClient) {
        this.villeClient = villeClient;
    }
    
    /**
     * Met à jour la liste des abonnements d'un client
     *
     * @param lesAbonnements la nouvelle liste d'abonnement
     */
    public void setLesAbonnements(List<Abonnement> lesAbonnements) {
        this.lesAbonnements = lesAbonnements;
    }
//</editor-fold>
  
    
//<editor-fold defaultstate="collapsed" desc="Méthode">
    /**
     * Ajoute un abonnement à la liste des abonnements du client
     *
     * @param a L'abonnement que l'on souhaite ajouter à la liste des abonnements
     */
    public void ajouterAbonnement(Abonnement a){
        lesAbonnements.add(a);
    }
//</editor-fold>
}
