package com.seg.gestconso.metier;

import java.util.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Une commune représente un lieu d'habitation, une ville.
 *
 * @author sio
 */
@Entity
public class Commune {

//<editor-fold defaultstate="collapsed" desc="Propriétés">
    /**
     * Numéro unique et définitif de la commune
     */
    @Id
    private int numCom;

    /**
     * Nom de la commune
     */
    @Column(length = 40, nullable = false)
    private String nomCom;

    /**
     * Secteurs de la commune
     */
    @OneToMany(mappedBy = "laCommune")
    @Column(nullable = false)
    private List<Secteur> lesSecteurs;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructeurs">
    /**
     * Crée une Commune
     */
    public Commune() {
        lesSecteurs = new ArrayList<>();
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Accesseurs">
    /**
     * Retourne le numéro de la commune
     *
     * @return Le numéro de la commune
     */
    public int getNumCom() {
        return numCom;
    }

    /**
     * Retourne le nom de la commune
     *
     * @return Le nom de la commune
     */
    public String getNomCom() {
        return nomCom;
    }

    /**
     * Retourne la liste des secteurs de la commune
     *
     * @return Liste de secteurs
     */
    public List<Secteur> getLesSecteurs() {
        return lesSecteurs;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Mutateurs">
    /**
     * Met à jour le numéro de la commune (méthode obligatoire pour le
     * fonctionnement d'Hibernate)
     *
     * @param numCom Le nouveau numéro de la commune.
     */
    public void setNumCom(int numCom) {
        this.numCom = numCom;
    }

    /**
     * Met à jour le nom de la commune
     *
     * @param nomCom Le nouveau nom de la commune.
     */
    public void setNomCom(String nomCom) {
        this.nomCom = nomCom;
    }

    /**
     * Met à jour la liste des secteurs de la commune
     *
     * @param lesSecteurs Liste de secteurs
     */
    public void setLesSecteurs(List<Secteur> lesSecteurs) {
        this.lesSecteurs = lesSecteurs;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Méthodes">
    /**
     * Ajoute un secteur à la liste
     *
     * @param s le secteur à ajouter
     */
    public void addSecteur(Secteur s) {
        lesSecteurs.add(s);
    }

    /**
     * Retourne le volumme total distribué en fonction du type : - Vanne -
     * Usager
     *
     * @param type le type de Volume total à récuperer (Vanne ou Usager)
     * @return le volume total distribué
     */
    public int volumeTotal(String type) {
        int total = 0;
        for (Secteur u : lesSecteurs) {
            for (Compteur c : u.getLesCompteurs()) {
                if (c.getClass().getSimpleName().equals(type)) {
                    total = total + c.consommation();
                }
            }
        }
        return total;
    }

    /**
     * retourne le volume d'eau perdu sur une commune
     *
     * @return le volume d'eau perdu sur une commune
     */
    public int perteEau() {
        int total;
        int vanne = volumeTotal("Vanne");
        int usager = volumeTotal("Usager");
        total = vanne - usager;
        return total;
    }
//</editor-fold>
}
