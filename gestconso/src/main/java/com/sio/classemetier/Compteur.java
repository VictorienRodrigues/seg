package com.seg.gestconso.metier;

import java.util.*;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.DATE;
import static javax.persistence.DiscriminatorType.STRING;
import static javax.persistence.InheritanceType.SINGLE_TABLE;

/**
 * Un compteur représente un appareil de relevés de consommation d'eau.
 *
 * @author sio
 */
@Entity
@Table(name = "Compteur")
@Inheritance(strategy = SINGLE_TABLE)
@DiscriminatorColumn(name = "typCompteur", discriminatorType = STRING)
public class Compteur {

//<editor-fold defaultstate="collapsed" desc="Propriétés">
    /**
     * Référence unique et définitive dde la vanne
     */
    @Id
    @Column(unique = true, length = 11)
    private String refCompteur;

    /**
     * Date d'installation du compteur
     */
    @Column(nullable = false)
    @Temporal(DATE)
    private Date dateInstallation;

    /**
     * Marque du compteur
     */
    @Column(length = 50, nullable = false)
    private String marque;

    /**
     * Liste des relevés du compteur
     */
    @OneToMany(mappedBy = "leCompteur")
    private List<Releve> lesReleves = new ArrayList<>();
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructeurs">
    /**
     * Crée un Compteur
     */
    public Compteur() {

    }

    /**
     * Crée un Compteur avec sa référence, sa date d'installation et sa
     * marque
     *
     * @param refCompteur Référence de la vanne
     * @param dateInstallation Date d'installation du compteur
     * @param marque Marque du compteur
     */
    public Compteur(String refCompteur, Date dateInstallation, String marque) {
        this.refCompteur = refCompteur;
        this.dateInstallation = dateInstallation;
        this.marque = marque;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Accesseurs">
    /**
     * Retourne la référence du compteur
     *
     * @return La référence du compteur
     */
    public String getRefCompteur() {
        return refCompteur;
    }

    /**
     * Retourne la date d'installation du compteur
     *
     * @return La date d'installation du compteur
     */
    public Date getDateInstallation() {
        return dateInstallation;
    }

    /**
     * Retourne la marque du compteur
     *
     * @return La marque du compteur
     */
    public String getMarque() {
        return marque;
    }

    /**
     * retourne les releves d'un compteur
     *
     * @return les releves d'un compteur
     */
    public List<Releve> getLesReleves() {
        return lesReleves;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Mutateurs">
    /**
     * Met à jour la date d'installation du compteur
     *
     * @param dateInstallation La nouvelle date d'installation du compteur
     */
    public void setDateInstallation(Date dateInstallation) {
        this.dateInstallation = dateInstallation;
    }

    /**
     * Met à jour la marque du compteur
     *
     * @param marque La nouvelle marque du compteur
     */
    public void setMarque(String marque) {
        this.marque = marque;
    }

    /**
     * Met à jour la liste des releves
     *
     * @param lesReleves nouvelle liste de relevés
     */
    public void setLesReleves(List<Releve> lesReleves) {
        this.lesReleves = lesReleves;
    }

    /**
     * Met à jour la reférence du compteur (méthode obligatoire pour le
     * fonctionnement d'Hibernate)
     *
     * @param refCompteur la nouvelle référence
     */
    public void setRefCompteur(String refCompteur) {
        this.refCompteur = refCompteur;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Méthodes">
    /**
     * Ajoute un Relevé à la liste
     *
     * @param r Le Relevé à ajouter
     */
    public void addReleve(Releve r) {
        lesReleves.add(r);
    }

    /**
     * Retourne la consommation totale en eau d'un compteur sur les deux derniers
     * Relevés ou s'il n'y en a qu'un la valeur de l'index du relevé
     *
     * @return la consommation totale en eau d'un compteur sur les deux derniers
     * relevés ou s'il n'y en a qu'un la valeur de l'index du relevé
     */
    public int consommation() {
        int total = 0;

        if (lesReleves.size() == 1) {
            total = lesReleves.get(0).getIndexReleve();
        } else if (lesReleves.size() > 1) {
            lesReleves.sort(Comparator.comparing(o -> o.getDateReleve()));
            total = lesReleves.get(lesReleves.size() - 1).getIndexReleve() - lesReleves.get(lesReleves.size() - 2).getIndexReleve();
        }
        return total;
    }
//</editor-fold>

}
