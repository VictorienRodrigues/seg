package com.seg.gestconso.metier;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.DATE;

/**
 * Un relevé représente un relevé de consommation d'eau d'un logement
 *
 * @author sio
 */
@Entity
public class Releve {

//<editor-fold defaultstate="collapsed" desc="Propriétés">
    /**
     * Numéro unique et définitif du relevé
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int numReleve;

    /**
     * Date de création du relevé
     */
    @Column(nullable = false)
    @Temporal(DATE)
    private Date dateReleve;

    /**
     * Volume d'eau relevé
     */
    private int indexReleve;

    /**
     * Le compteur concerné par le relevé
     */
    @ManyToOne
    @JoinColumn(name = "idCompteur")
    private Compteur leCompteur;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructeur">
    /**
     * Crée un relevé
     */
    public Releve() {
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Accesseurs">
    /**
     * Retourne le numéro du relevé
     *
     * @return Le numéro du relevé
     */
    public int getNumReleve() {
        return numReleve;
    }
    
    /**
     * Retourne la date du relevé
     *
     * @return La date du relevé
     */
    public Date getDateReleve() {
        return dateReleve;
    }
    
    /**
     * Retourne l'index du relevé, c'est à dire le volume d'eau consommé à cette
     * date
     *
     * @return L'index du relevé
     */
    public int getIndexReleve() {
        return indexReleve;
    }
    
    /**
     * Retourne le compteur (Usager ou Vanne)
     * @return Le compteur concerné par le relevé
     */
    public Compteur getLeCompteur() {
        return leCompteur;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Mutateurs">
    /**
     * Met à jour le numéro de relevé (méthode obligatoire pour le
     * fonctionnement d'Hibernate)
     *
     * @param numReleve Le nouveau numéro de relevé
     */
    public void setNumReleve(int numReleve) {
        this.numReleve = numReleve;
    }
    
    /**
     * Met à jour la date de création du relevé
     *
     * @param dateReleve La nouvelle date de relevé
     */
    public void setDateReleve(Date dateReleve) {
        this.dateReleve = dateReleve;
    }
    
    /**
     * Met à jour l'index du compteur du relevé
     *
     * @param indexReleve le volume d'eau consommé
     */
    public void setIndexReleve(int indexReleve) {
        this.indexReleve = indexReleve;
    }
    
    
    /**
     * Met à jour le compteur concerné par le relevé
     * @param leCompteur Compteur (usager ou vanne) du relevé
     */
    public void setLeCompteur(Compteur leCompteur) {
        this.leCompteur = leCompteur;
    }
//</editor-fold>
}
