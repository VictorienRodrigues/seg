package com.seg.gestconso.metier;

import java.util.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Un secteur représente une zone géographique, ou quartier, d'une commune Une
 * commune a plusieurs secteurs
 *
 * @author sio
 */
@Entity
public class Secteur {

//<editor-fold defaultstate="collapsed" desc="Propriétés">
    /**
     * Numéro unique et définitif du secteur
     */
    @Id
    private int numSecteur;

    /**
     * Nom du quartier/secteur
     */
    @Column(length = 30)
    private String nomSecteur;

    /**
     * La présence ou non d'au moins un espace vert dans le secteur La valeur 1
     * vaut Oui.
     */
    private Boolean espaceVert;

    /**
     * La commune dans laquelle se situe le secteur
     */
    @ManyToOne
    @JoinColumn(name = "numCommune")
    private Commune laCommune;

    /**
     * Liste des compteurs présents dans le secteur
     */
    @OneToMany
    @JoinColumn(name = "numSecteur")
    private List<Compteur> lesCompteurs = new ArrayList();
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructeur">
    /**
     * Crée un secteur
     */
    public Secteur() {
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Accesseurs">
    /**
     * Retourne le numéro du secteur
     *
     * @return Le numéro du secteur
     */
    public int getNumSecteur() {
        return numSecteur;
    }

    /**
     * Retourne le nom du secteur
     *
     * @return Le nom du secteur
     */
    public String getNomSecteur() {
        return nomSecteur;
    }

    /**
     * Retourne la présence ou non d'un espace vert dans le secteur
     *
     * @return La présence ou non d'un espace vert dans le secteur
     */
    public Boolean getEspaceVert() {
        return espaceVert;
    }

    /**
     * Retourne la commune dans laquelle se situe le secteur
     *
     * @return La commune dans laquelle se situe le secteur
     */
    public Commune getLaCommune() {
        return laCommune;
    }

    /**
     * Retourne la liste des compteurs du secteur
     *
     * @return La liste des compteurs du secteur
     */
    public List<Compteur> getLesCompteurs() {
        return lesCompteurs;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Mutateurs">
    /**
     * Met à jour le numéro du secteur
     *
     * @param numSecteur Le nouveau numéro de secteur
     */
    public void setNumSecteur(int numSecteur) {
        this.numSecteur = numSecteur;
    }

    /**
     * Met à jour le nom du secteur
     *
     * @param nomSecteur Le nouveau nom du secteur
     */
    public void setNomSecteur(String nomSecteur) {
        this.nomSecteur = nomSecteur;
    }

    /**
     * Met à jour la présence ou non d'un espace vert dans le secteur
     *
     * @param espaceVert La présence ou non d'un espace vert dans le secteur
     */
    public void setEspaceVert(Boolean espaceVert) {
        this.espaceVert = espaceVert;
    }

    /**
     * Met à jour la commune dans laquelle se situe le secteur
     *
     * @param laCommune La nouvelle commune dans laquelle se situe le secteur
     */
    public void setLaCommune(Commune laCommune) {
        this.laCommune = laCommune;
    }

    /**
     * Met à jour la liste des compteurs du secteur
     *
     * @param lesCompteurs La nouvelle liste des compteurs du secteur
     */
    public void setLesCompteurs(List<Compteur> lesCompteurs) {
        this.lesCompteurs = lesCompteurs;
    }
//</editor-fold>

    /**
     * Ajoute un compteur à la liste des compteurs
     *
     * @param c Le compteur à ajouter à la liste des compteurs du secteur
     */
    public void addCompteur(Compteur c) {
        this.lesCompteurs.add(c);
    }
}
