package com.seg.gestconso.metier;

import javax.persistence.*;

/**
 * Compteur chez un particulier
 *
 * @author sio
 */
@Entity
@DiscriminatorValue(value = "usager")
public class Usager extends Compteur {

    /**
     * L'abonnement du compteur usager
     */
    @ManyToOne
    @JoinColumn(name = "refAbo")
    private Abonnement lAbonnement;

    /**
     * Crée un compteur Usager
     */
    public Usager() {
        super();
    }

//<editor-fold defaultstate="collapsed" desc="Accesseurs">
    /**
     * Retourne l'abonnement de l'usager
     *
     * @return l'abonnement correspondant au compteur
     */
    public Abonnement getlAbonnement() {
        return lAbonnement;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Mutateurs">
    /**
     * Met à jour l'abonnement de l'usager
     *
     * @param lAbonnement l'abonnement de l'usager
     */
    public void setlAbonnement(Abonnement lAbonnement) {
        this.lAbonnement = lAbonnement;
    }
//</editor-fold>
}
