package com.seg.gestconso.metier;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Une vanne représente le moyen d'ouverture et de fermeture d'un compteur
 *
 * @author sio
 */
@Entity
@DiscriminatorValue(value = "vanne")
public class Vanne extends Compteur {

    /**
     * Crée un compteur Vanne
     */
    public Vanne() {
        super();
    }
}
